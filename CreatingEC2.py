# -*- coding: utf-8 -*-
"""
Created on Wed Jul 18 21:22:19 2018

@author: jichen
"""

import boto3
import paramiko
import os
import time
ec2 = boto3.resource('ec2')
clientcon = boto3.client('ec2')

#------- creating linux server----------------------#
"""
before running create_instances, please make sure that following items has been changed based on your requirement
--->ImageId
--->InstanceType
--->KeyName
--->Security Group
--->SubnetId
Please add more parameters if you have specific demands

"""
instance = ec2.create_instances(
       ImageId='ami-ed838091',
       MinCount=1,
       MaxCount=1,
       InstanceType='t2.micro',
       KeyName = 'RDSnetworktesting',
       NetworkInterfaces=[
           {
               'DeviceIndex': 0,    
               'AssociatePublicIpAddress': True, 
               'Groups':['sg-0009f3ebe3b66e88d'],
               'DeleteOnTermination': True,
               'Description': 'Server generated from Python',
               'SubnetId': 'subnet-0a71aef15ec1a7775'
           },
       ],)
print(instance[0].id)

status = 'pending'
while status!='running':
     status = clientcon.describe_instances(InstanceIds=[instance[0].id])['Reservations'][0]['Instances'][0]['State']['Name']
     time.sleep(20)
     print(status)
#pending for server to finish creation, usually it will take 10s to be recognized by SSH
print('creating')
time.sleep(30)
MyIP = clientcon.describe_instances(InstanceIds=[instance[0].id])['Reservations'][0]['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp']

#------- Connecting to Linux server----------------------#
"""
before running create_instances, please make sure that following items has been changed based on your requirement
--->key_filename

"""
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(MyIP, username='ec2-user', key_filename='D:\Tooltesting\RDSnetworktesting-openssh')

#------- Installing docker in Linux server----------------------#
print('sudo yum update -y')
stdin, stdout, stderr = ssh.exec_command('sudo yum update -y')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('sudo yum install -y docker')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('sudo service docker start')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('sudo docker info')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('sudo usermod -aG docker ec2-user')
print(stdout.readlines())

#------- Installing git to pull index.html from repo----------------------#
stdin, stdout, stderr = ssh.exec_command('sudo yum install git')
stdin.write("yes\n")
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('git --version')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('sudo mkdir ~/docker-nginx/html')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('cd  ~/docker-nginx/html')
print(stdout.readlines())
stdin, stdout, stderr = ssh.exec_command('git clone https://bitbucket.org/CGjichen/hyperanna.git')
print(stdout.readlines())

#------- Launching Ngnix in docker----------------------#
stdin, stdout, stderr = ssh.exec_command('sudo docker run --name docker-nginx -p 80:80 -d -v ~/hyperanna/:/usr/share/nginx/html nginx')
print(stdout.readlines())

#------- You can check browser by typing the public IP in it----------------------#

#------- System pause, after typing anything, it will start terminate server----------------------#
os.system('PAUSE')
ssh.close()
result = clientcon.terminate_instances(InstanceIds=[instance[0].id])
print(result)