# HyperAnna Project

This readme is to give torturial about how to use python script to generate EC2, run docker and ngnix and destroy EC2

## Getting Started

Please make sure that your computer should install AWS CLI.
Also, you should have AWS account, key pairs. This user should have privilege to read and write EC2.

### Prerequisites

Go to "cmd" and type the command:

```
$ aws configure
```
In the command, you need to type in 4 parts:

```
AWS Access Key ID
AWS Secret Access Key
Default region name
Default output format
```
Then, you should be able use AWS in Python now

### Installing

Go to Anaconda powershell, before running the python script, you need to install boto3 and paramiko

```
$ pip install boto3
$ pip install paramiko
```
If they are successfully installed, you should be able to run python script

## Running Python script

There are some variables needed to change before running the script. 
I have already listed them on the top of the script. 
After changing those parameters, type command

```
$ python CreatingEC2.py
```
it will start running

## Browsing the webpage

When there is a black command window jumping out, DO NOT close it. Otherwise, it will start terminating the server
Go to the browser and type in the public IP
You should be able to see the "hello world" web page
